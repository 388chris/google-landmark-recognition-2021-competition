import os
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Model, load_model
from Model_Struct import *
from lib_modules.Parameters import GlobalParameters
from lib_modules.data_generator import DataGenerator
from lib_models.Arc_Loss import ArcLoss
#from lib_modules.CSVLoader import dataLoader

def Csv2DataFrame(datasetdir):
    label = {}
    df = pd.read_csv(datasetdir,dtype=str).drop(['Unnamed: 0'],axis=1)
    index = 0
    for i in df['label'].unique():
            label[i] = index
            index += 1
    ChangeList = df.values.tolist()
    return ChangeList, label


if __name__ == "__main__":
    TrainList, labels = Csv2DataFrame(GlobalParameters.Traindircsvloc)
    print(TrainList)

    
    train_generator = DataGenerator(path = TrainList, 
                                    batch_size = GlobalParameters.batch_size,
                                    size = GlobalParameters.imageshape,
                                    labelinput = labels,
                                    Shuffle=True)
    print("Total Data : ",train_generator.n)
    print(train_generator.labelinput)

    
    #model = ResNeXT(Shape = GlobalParameters.imageshape,num_classes = len(labels),BN=True,act=mish)
    model = ResNeXTArcLoss(Shape = GlobalParameters.imageshape,num_classes = len(labels),BN=True,act=mish)
    model.summary()

    # 編譯模型
    # 選用Adam為optimizer
    learning_rate = 10e-3
    #optimizer = keras.optimizers.RMSprop(lr=learning_rate, rho=0.9, epsilon=None, decay=0.0)
    optimizer = keras.optimizers.Adam(lr=learning_rate)

    #model.compile(loss='categorical_crossentropy',optimizer=optimizer,metrics=['accuracy'])
    model.compile(loss=ArcLoss(),optimizer=optimizer,metrics=['accuracy'])
    Reduce = tf.keras.callbacks.EarlyStopping(monitor='val_accuracy',patience=30,restore_best_weights=True)

    # save model
    modelfiles = './OwnGeneratortest.h5'
    model_mckp = tf.keras.callbacks.ModelCheckpoint(modelfiles,
                            monitor='val_accuracy',
                            save_best_only=True)

    callbacks_list = [Reduce, model_mckp]

    STEP_SIZE_TRAIN=train_generator.n//train_generator.batch_size
    #STEP_SIZE_VALID=test_generator.n//test_generator.batch_size

    cnn_result = model.fit(train_generator,
                        steps_per_epoch=STEP_SIZE_TRAIN,
                        callbacks=callbacks_list,
                        #validation_data=test_generator,
                        #validation_steps=STEP_SIZE_VALID,
                        epochs=GlobalParameters.epochs,
                        verbose=1)
   
    
