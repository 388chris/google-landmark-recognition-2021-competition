from lib_modules.Parameters import DataParameters, ModelStructParameters, ModelCompileParameters
from Training import TrainingOption
from Testing import TestingOption
import argparse
import os
import json

def get_parser():
    parser = argparse.ArgumentParser(description='GLR2021 Training Testing function')
    parser.add_argument('--t',default='train',type=str)
    return parser


if __name__ == "__main__":
    parser = get_parser()
    args = parser.parse_args()
    if args.t == "train":
        mainfunction = TrainingOption(SaveJsondir = DataParameters.SaveJson,
                                      owngenerator = DataParameters.owngenerator, 
                                      img_shape = DataParameters.imageshape, 
                                      batch_size = DataParameters.batch_size,)

        mainfunction.DataGeneratorSet(TrainCsv = DataParameters.Traindircsvloc, 
                                    ValidCsv = DataParameters.Testdircsvloc,
                                    shuffle=DataParameters.shuffle)

        mainfunction.Model_Struct_Parameters(localattention = ModelStructParameters.localattention,
                                        modelfiles = DataParameters.modelname,
                                        localattmodelname = DataParameters.localattmodelname,
                                        modelChoice = ModelStructParameters.modelChoice,
                                        GEM = ModelStructParameters.GEM,
                                        PoolType = ModelStructParameters.PoolType,
                                        BN = ModelStructParameters.BatchNor,
                                        act = ModelStructParameters.act)

        mainfunction.training(MultiGPU = ModelCompileParameters.MultiGPU,
                            epoch = ModelCompileParameters.epochs,
                            learning_rate = ModelCompileParameters.learning_rate,
                            loss = ModelCompileParameters.loss,
                            optimizerchoice = ModelCompileParameters.optimizerchoice)
    elif args.t == 'test':
        with open(DataParameters.jsonfiletest) as f:
            data = json.load(f)
        mainfunction = TestingOption(img_shape=data['imageshape'],batch_size=1)
        mainfunction.DataGeneratorSet(TrainCsv=DataParameters.Traindircsvloc, 
                                      ValidCsv=DataParameters.Testdircsvloc)
        mainfunction.testing(modelfiles = data['modelname'])
        mainfunction.GAPValidation()
    
    
