import os
import numpy as np
import pandas as pd
import tensorflow as tf
from lib_modules.data_generator import ValidDataGenerator
from Model_Struct import *
from lib_models.Arc_Loss import ArcLayer, ArcLoss
from lib_models.mish_act import mish
from lib_modules.GAP import global_average_precision_score
from tensorflow.keras import backend as K
import gc


class TestingOption():
    def __init__(self, img_shape=224, batch_size = 32):
        self.img_shape = img_shape
        self.batch_size = batch_size

    def Csv2DataFrame(self,Datadircsvloc):
        dffordict = pd.read_csv(Datadircsvloc,dtype=str, index_col=0)
        df = pd.read_csv(Datadircsvloc,dtype=str)
        DatawithLabel = dffordict.to_dict()['label']
        index = 0
        label = {}
        for i in df['label'].unique():
                label[i] = index
                index += 1
        ChangeList = df.values.tolist()
        outputlabel = {v: k for k, v in label.items()}
        return ChangeList, outputlabel, DatawithLabel


    def DataGeneratorSet(self,TrainCsv, ValidCsv):
        _, self.labels, _ = self.Csv2DataFrame(TrainCsv)
        self.ValidList, _ ,self.DatawithLabel= self.Csv2DataFrame(ValidCsv)
        #self.ValidList = self.ValidList[:32]
        #self.DatawithLabel = dict(itertools.islice(self.DatawithLabel.items(), 32))
        #self.Test_generator = ValidDataGenerator(path = self.ValidList, batch_size = self.batch_size, size = self.img_shape)

    def testing(self,modelfiles = 'EffiecientB7Pre.h5'):
        n = 5000
        output=[self.ValidList[i:i + n] for i in range(0, len(self.ValidList), n)]
        for i in range(len(output)):
            Arcmodel = load_model(modelfiles,compile=False,custom_objects={'mish':mish,
                                                                          'ArcLayer':ArcLayer,
                                                                          'ArcLoss':ArcLoss,
                                                                          'GeM':GeM,
                                                                          'PReLU':tf.keras.layers.PReLU()})         
            Test_generator = ValidDataGenerator(path = output[i], batch_size = self.batch_size, size = self.img_shape)
            Answer = Arcmodel.predict(Test_generator)
            categorylist = [self.labels[np.argmax(x)] for x in Answer]
            scorelist = [x[np.argmax(x)] for x in Answer]
            categorylist = np.array(categorylist)
            scorelist = np.array(scorelist)

            if i == 0:
                self.categoryconcat = categorylist
                self.scoreconcat = scorelist
            else:
                self.categoryconcat = np.concatenate((self.categoryconcat, categorylist), axis=0)
                self.scoreconcat = np.concatenate((self.scoreconcat, scorelist), axis=0)
            print("Finish %d"%(i))
            del Arcmodel
            del Answer
            del categorylist
            del scorelist

            K.clear_session()
            gc.collect()


        
    def GAPValidation(self):
        predicted_label = {}
        for i in range(len(self.scoreconcat)):
            predicted_label[self.ValidList[i][0]] = [self.categoryconcat[i],self.scoreconcat[i]]
        Score = global_average_precision_score(self.DatawithLabel,predicted_label)
        print("The GAP score = ",Score)



    
