import os
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from lib_modules.data_generator import DataGenerator
from lib_modules.Ram_Clean import RamCleaning
from lib_modules.ParameterJson import DicttoJson
from Model_Struct import *


class TrainingOption():
    '''
    This is a pipeline of Training process, include input data , read data , data preprocessing and modeling
    '''
    def __init__(self,SaveJsondir,owngenerator = False, img_shape=224, batch_size = 32):
        '''
        SaveJsondir : str type, the input json directory, not include json file name
        owngenerator : bool type, to check use own training generator or keras generator
        img_shape : int type, image input shape, ex : 224
        batch_size : int type, image training input batch size, ex : 32
        '''
        self.saveJsondir = SaveJsondir
        self.owngenerator = owngenerator
        self.img_shape = img_shape
        self.batch_size = batch_size
        
    def Csv2DataFrame(self,Datadircsvloc):
        '''
        Datadircsvloc : str type, the input csv
        return the label index and pandas csv file
        '''
        df = pd.read_csv(Datadircsvloc,dtype=str)
        if self.owngenerator == True:
            index = 0
            label = {}
            for i in df['label'].unique():
                    label[i] = index
                    index += 1
            ChangeList = df.values.tolist()
            return ChangeList, label
        else:
            return df

    def DataGeneratorSet(self,TrainCsv, ValidCsv, arc = False, shuffle=True):
        '''
        TrainCsv : str type, input train csv file name
        ValidCsv : str type, input valid csv file name 
        shuffle : bool type, training set shuffle, true for shuffle, false for not shuufle
        '''
        if self.owngenerator == True:
            self.TrainList, self.labels = self.Csv2DataFrame(TrainCsv)
            self.ValidList, _ = self.Csv2DataFrame(ValidCsv)
            
            self.train_generator = DataGenerator(path = self.TrainList, 
                                batch_size = self.batch_size,
                                size = self.img_shape,
                                labelinput = self.labels,
                                Shuffle=shuffle)
            
            self.valid_generator = DataGenerator(path = self.ValidList, 
                    batch_size = self.batch_size,
                    size = self.img_shape,
                    labelinput = self.labels,
                    Shuffle=False)            
            

            print("Total Train data : ",self.train_generator.n)
            print("Total Valid data : ",self.valid_generator.n)
            print("Total labels : ",self.labels)
        else:
            self.TrainList = self.Csv2DataFrame(TrainCsv)
            self.ValidList = self.Csv2DataFrame(ValidCsv)
            
            # Train generator
            train_datagen = ImageDataGenerator(rescale = 1./255,
                            horizontal_flip=True,
                            shear_range=0.2,
                            zoom_range=0.2)
            
            self.train_generator = train_datagen.flow_from_dataframe(
                dataframe = self.TrainList,
                directory = '',
                x_col = 'img_path',
                y_col = 'label',
                color_mode='rgb',
                target_size = (self.img_shape,self.img_shape),
                batch_size = self.batch_size,
                shuffle = shuffle,
                class_mode = 'categorical')
            
            # Valid generator
            valid_datagen = ImageDataGenerator(rescale = 1./255,)
            self.valid_generator = valid_datagen.flow_from_dataframe(
                dataframe = self.ValidList,
                directory = '',
                x_col = 'img_path',
                y_col = 'label',
                color_mode='rgb',
                target_size = (self.img_shape,self.img_shape),
                batch_size = self.batch_size,
                shuffle = False,
                class_mode = 'categorical')
            self.labels = self.train_generator.class_indices
            print(self.labels)
            
    def Model_Struct_Parameters(self,localattention = False,modelfiles = 'ResNeXTArcLossGEMBest.h5',localattmodelname = 'ResNeXTArcLossGEMLocal.h5',modelChoice=PreEffiecientB7,GEM = True,PoolType = 'GAP',BN=False,act='relu'):
        '''
        modelfiles : str type, save model name, ex : 'EffiecientB7Pre.h5'
        modelChoice : function type, choice which model to use in Model_Struct.py, ex : PreEffiecientB7
        GEM : bool type, true for using GeM layer
        PoolType : str type, if not using GEM, choose 'GAP' or 'GMP' for flatten
        BatchNor : bool type, true for using batch normalization
        act : str type, keras activation function, default : 'relu'
        '''
        self.localattention = localattention
        self.modelfiles = modelfiles
        self.localattmodelname = localattmodelname
        self.modelChoice = modelChoice
        self.GEM = GEM
        self.PoolType = PoolType
        self.BN = BN
        self.act = act

    def Model_Struct_Choose(self):
  
        if self.localattention == True:
            model = LocalAttentionFinetune(modelfiles = self.modelfiles,
                                           num_classes = len(self.labels),
                                           GEM = self.GEM,
                                           PoolType = self.PoolType,
                                           BN=self.BN,act=self.act)
            self.modelfiles = self.localattmodelname
        else:
            model = self.modelChoice(Shape = self.img_shape,
                                     num_classes = len(self.labels),
                                     GEM = self.GEM,
                                     PoolType = self.PoolType,
                                     BN=self.BN,
                                     act=self.act)
        return model    

    def training(self,MultiGPU = False,epoch=50,learning_rate=1e-3,loss='categorical_crossentropy',optimizerchoice='sgd'):
        '''
        MultiGPU : bool type, true for multigpu and false for single gpu
        epoch : int type, training epoch, ex : 50
        learning_rate : float type, optimization learning rate, default : 1e-3
        loss : str type, training loss function, Arc layer with ArcLoss and other with keras loss function ,ex : 'categorical_crossentropy'
        optimizerchoice : str type, keras optimizer function, default : 'sgd'
        '''
       
        if os.path.exists(self.saveJsondir) == False:
            os.makedirs(self.saveJsondir)
        _, modelfilename = os.path.split(self.modelfiles)
        Outputjsonfile = DicttoJson(inputshape = self.img_shape,
                                    modelname = self.modelfiles,
                                    GEM = self.GEM,
                                    PoolType = self.PoolType,
                                    act = self.act,)
        Outputjsonfile.Outputfile(self.saveJsondir+'/'+modelfilename[:-3]+'.json')  

        if MultiGPU == True:
            strategy = tf.distribute.MirroredStrategy()
            print('Number of devices: {}'.format(strategy.num_replicas_in_sync))

            # Open a strategy scope.
            with strategy.scope():
                model = self.Model_Struct_Choose()
                # model compile
                if optimizerchoice == 'adam':
                    optimizer = tf.keras.optimizers.Adam(lr=learning_rate)
                elif optimizerchoice == 'sgd':
                    optimizer = tf.keras.optimizers.SGD(learning_rate=learning_rate, momentum=.90)
                elif optimizerchoice == 'RMSprop':
                    optimizer = keras.optimizers.RMSprop(lr=learning_rate, rho=0.9, epsilon=None, decay=0.0)
                    
                if self.localattention == True:
                    model.compile(loss='categorical_crossentropy',optimizer=optimizer,metrics=['accuracy'])
                else:
                    model.compile(loss=loss,optimizer=optimizer,metrics=['accuracy'])
        else:
            model = self.Model_Struct_Choose()
            # model compile
            if optimizerchoice == 'adam':
                optimizer = tf.keras.optimizers.Adam(lr=learning_rate)
            elif optimizerchoice == 'sgd':
                optimizer = tf.keras.optimizers.SGD(learning_rate=learning_rate, momentum=.90)
            elif optimizerchoice == 'RMSprop':
                optimizer = keras.optimizers.RMSprop(lr=learning_rate, rho=0.9, epsilon=None, decay=0.0)

            if self.localattention == True:
                model.compile(loss='categorical_crossentropy',optimizer=optimizer,metrics=['accuracy'])
            else:
                model.compile(loss=loss,optimizer=optimizer,metrics=['accuracy'])              
        Reduce = tf.keras.callbacks.EarlyStopping(monitor='val_accuracy',patience=30,restore_best_weights=True)
        
        model.summary()
        # save model
        print("Model name : %s"%(self.modelfiles))
        
        model_mckp = tf.keras.callbacks.ModelCheckpoint(self.modelfiles,
                                monitor='val_accuracy',
                                save_best_only=True)

        callbacks_list = [Reduce, model_mckp, RamCleaning()]

        STEP_SIZE_TRAIN=self.train_generator.n//self.train_generator.batch_size
        STEP_SIZE_VALID=self.valid_generator.n//self.valid_generator.batch_size

        model.fit(self.train_generator,
                steps_per_epoch=STEP_SIZE_TRAIN,
                callbacks=callbacks_list,
                validation_data=self.valid_generator,
                validation_steps=STEP_SIZE_VALID,
                epochs=epoch,
                verbose=1)



    
