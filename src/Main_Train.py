import os
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Model, load_model
from Model_Struct import *
from lib_modules.Parameters import GlobalParameters
from lib_modules.data_generator import DataGenerator
from lib_models.Arc_Loss import ArcLoss
#from lib_modules.CSVLoader import dataLoader

def Csv2DataFrame(datasetdir):
    df = pd.read_csv(datasetdir,dtype=str).drop(['Unnamed: 0'],axis=1)
    return df


if __name__ == "__main__":
    TrainList= Csv2DataFrame(GlobalParameters.Traindircsvloc)
    print(TrainList)

    train_datagen = ImageDataGenerator(rescale = 1./255,
                               horizontal_flip=True,
                               shear_range=0.2,
                               zoom_range=0.2)

    train_generator = train_datagen.flow_from_dataframe(
        dataframe = TrainList,
        directory = '',
        x_col = 'img_path',
        y_col = 'label',
        color_mode='rgb',
        target_size = (GlobalParameters.imageshape,GlobalParameters.imageshape),
        batch_size = GlobalParameters.batch_size,
        shuffle = True,
        class_mode = 'categorical')

    labels = train_generator.class_indices
    print(train_generator.n)
    print(labels)

    
    #model = ResNeXT(Shape = GlobalParameters.imageshape,num_classes = len(labels),BN=True,act=mish)
    model = ResNeXTArcLoss(Shape = GlobalParameters.imageshape,num_classes = len(labels),BN=True,act=mish)
    model.summary()

    # 編譯模型
    # 選用Adam為optimizer
    learning_rate = 10e-3
    #optimizer = keras.optimizers.RMSprop(lr=learning_rate, rho=0.9, epsilon=None, decay=0.0)
    optimizer = keras.optimizers.Adam(lr=learning_rate)

    #model.compile(loss='categorical_crossentropy',optimizer=optimizer,metrics=['accuracy'])
    model.compile(loss=ArcLoss(),optimizer=optimizer,metrics=['accuracy'])
    Reduce = tf.keras.callbacks.EarlyStopping(monitor='val_accuracy',patience=30,restore_best_weights=True)

    # save model
    modelfiles = './OwnGeneratortest.h5'
    model_mckp = tf.keras.callbacks.ModelCheckpoint(modelfiles,
                            monitor='val_accuracy',
                            save_best_only=True)

    callbacks_list = [Reduce, model_mckp]

    STEP_SIZE_TRAIN=train_generator.n//train_generator.batch_size
    #STEP_SIZE_VALID=test_generator.n//test_generator.batch_size

    cnn_result = model.fit(train_generator,
                        steps_per_epoch=STEP_SIZE_TRAIN,
                        callbacks=callbacks_list,
                        #validation_data=test_generator,
                        #validation_steps=STEP_SIZE_VALID,
                        epochs=GlobalParameters.epochs,
                        verbose=1)
    
    
