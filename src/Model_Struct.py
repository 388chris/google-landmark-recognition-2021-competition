import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import  GlobalAveragePooling2D, MaxPooling2D, GlobalMaxPooling2D, Reshape
from tensorflow.keras.layers import Conv2D, Dense, Activation, BatchNormalization, Flatten
from tensorflow.keras.layers import Softmax
from tensorflow.keras.models import Model, load_model
import tensorflow.keras.backend as K
from lib_models.Global_context_block import global_context_block
from lib_models.Self_Attention import selfattention
from lib_models.ResidualSE import ResidualIConvSE,ResidualIdentitySE
from lib_models.ResidualXT import ResidualIConvXT,ResidualIdentityXT
from lib_models.Arc_Loss import ArcLayer, ArcLoss
from lib_models.Gem import GeM
from lib_models.Focal_Loss import categorical_focal_loss
from lib_models.mish_act import mish
import math

    
def LocalAttentionFinetune(modelfiles = 'ResNeXTArcLossGEMBest.h5',num_classes = 10,GEM = True,PoolType = 'GAP',BN=False,act='relu'):
    '''
    Delf model

    This is a model structure of local attention finetune function, input a backbone pretrained model and output model Structure.
    return model = Model(inputs = Backbone.inputs, outputs = logits)

    parameters discriptions
    - Shape => int type, input shape, ex : 224
    - num_classes => int type, number of class, ex : 10
    - GEM => bool type, use GeM layer or not, ex : True
    - PoolType => str type, if not using GEM , the flatten of using 'GAP' or 'GMP'
    - BN => bool type, using batch normalization or not
    - act => str type, activation function name, use keras activation function name or custom function mish
    '''
    Backbone = load_model(modelfiles,compile=False,custom_objects={'mish':mish,
                                                                  'ArcLayer':ArcLayer,
                                                                  'ArcLoss':ArcLoss,
                                                                  'GeM' : GeM,
                                                                  'PReLU':tf.keras.layers.PReLU()})
    Backbone.trainable = False
    #Feature_Extract = Backbone.get_layer('activation_12').output      #Residual Block 2 of ResidualIConvXT
    Feature_Extract = Backbone.get_layer('activation_24').output      #Residual Block 3 of ResidualIConvXT
    _,_,_,channels = Feature_Extract.get_shape().as_list()

    #down sampling input features, multiply using
    target = Conv2D(filters=128,kernel_size=(1,1),strides=(1,1), padding='same',name='input_Feautures')(Feature_Extract) 

    # One convlution layer
    layer = Conv2D(filters=512,kernel_size=(1,1),strides=(1,1), padding='same',name='conv_Feautures')(Feature_Extract)
    if BN == True:
        layer = BatchNormalization(name = 'BN_conv_Features')(layer)
    layer = Activation('relu',name='conv_Features_act')(layer)

    # Pixel wise self attention
    layer = Conv2D(filters=1,kernel_size=(1,1),strides=(1,1), padding='same',name='conv_Feauturespixel')(layer) #(32,32,1)
    layer = Activation('softplus',name='conv_Feauturespixel_output')(layer)

    prob = tf.nn.l2_normalize(layer, axis=1,name='final_l2_normalization') #L2 normalization
    feat = tf.reduce_mean(tf.multiply(target, prob), [1, 2], keepdims=False) # multiply probability to input features (32,32,512) and reduce_mean (512)

    logits = Dense(num_classes,activation='softmax')(feat)

    model = Model(inputs = Backbone.inputs, outputs = logits)
    model.summary()
    return model

def PreDenseNet(Shape,num_classes,GEM = True,PoolType = 'GAP',BN=False,act='relu'):
    '''
    This is a model structure function, input a backbone and output model Structure.
    return Model(inputs = PreEff.inputs, outputs = logits)

    parameters discriptions
    - Shape => int type, input shape, ex : 224
    - num_classes => int type, number of class, ex : 10
    - GEM => bool type, use GeM layer or not, ex : True
    - PoolType => str type, if not using GEM , the flatten of using 'GAP' or 'GMP'
    - BN => bool type, using batch normalization or not
    - act => str type, activation function name, use keras activation function name or custom function mish
    '''
    PreEff = tf.keras.applications.DenseNet121(include_top=False,weights="imagenet",input_shape=(Shape,Shape,3),)
    PreEff.trainable = False
    _,H,W,_ = PreEff.output.get_shape().as_list()
    if GEM == True:
        layer = GeM(pool_size=H)(PreEff.output)
        x = Flatten()(layer)
    elif GEM ==False:
        if PoolType == 'GAP':
            x = GlobalAveragePooling2D()(PreEff.output)
        elif PoolType == 'GMP':
            x = GlobalMaxPooling2D()(PreEff.output)
    dense = Dense(512)(x)
    if BN == True:
        dense = tf.keras.layers.BatchNormalization()(dense)
    dense = Activation(tf.keras.layers.PReLU())(dense)
    logits = ArcLayer(units = num_classes)(dense)
    
    model = Model(inputs = PreEff.inputs, outputs = logits)
    return  model

def PreEffiecientB3(Shape,num_classes,GEM = True,PoolType = 'GAP',BN=False,act='relu'):
    '''
    This is a model structure function, input a backbone and output model Structure.
    return Model(inputs = PreEff.inputs, outputs = logits)

    parameters discriptions
    - Shape => int type, input shape, ex : 224
    - num_classes => int type, number of class, ex : 10
    - GEM => bool type, use GeM layer or not, ex : True
    - PoolType => str type, if not using GEM , the flatten of using 'GAP' or 'GMP'
    - BN => bool type, using batch normalization or not
    - act => str type, activation function name, use keras activation function name or custom function mish
    '''

    PreEff = tf.keras.applications.EfficientNetB3(include_top=False,weights="imagenet",input_shape=(Shape,Shape,3),)
    PreEff.trainable = False
    _,H,W,_ = PreEff.output.get_shape().as_list()
    if GEM == True:
        layer = GeM(pool_size=H)(PreEff.output)
        x = Flatten()(layer)
    elif GEM ==False:
        if PoolType == 'GAP':
            x = GlobalAveragePooling2D()(PreEff.output)
        elif PoolType == 'GMP':
            x = GlobalMaxPooling2D()(PreEff.output)
    dense = Dense(512)(x)
    if BN == True:
        dense = tf.keras.layers.BatchNormalization()(dense)
    dense = Activation(tf.keras.layers.PReLU())(dense)
    logits = ArcLayer(units = num_classes)(dense)
    
    model = Model(inputs = PreEff.inputs, outputs = logits)
    return  model

def PreEffiecientB7(Shape,num_classes,GEM = True,PoolType = 'GAP',BN=False,act='relu'):
    '''
    This is a model structure function, input a backbone and output model Structure.
    return Model(inputs = PreEff.inputs, outputs = logits)

    parameters discriptions
    - Shape => int type, input shape, ex : 224
    - num_classes => int type, number of class, ex : 10
    - GEM => bool type, use GeM layer or not, ex : True
    - PoolType => str type, if not using GEM , the flatten of using 'GAP' or 'GMP'
    - BN => bool type, using batch normalization or not
    - act => str type, activation function name, use keras activation function name or custom function mish
    '''

    PreEff = tf.keras.applications.EfficientNetB7(include_top=False,weights="imagenet",input_shape=(Shape,Shape,3),)
    label = Input(shape=(num_classes,))
    PreEff.trainable = False #每一層全打開是 fine tune
    _,H,W,_ = PreEff.output.get_shape().as_list()
    if GEM == True:
        layer = GeM(pool_size=H)(PreEff.output)
        x = Flatten()(layer)
    elif GEM ==False:
        if PoolType == 'GAP':
            x = GlobalAveragePooling2D()(PreEff.output)
        elif PoolType == 'GMP':
            x = GlobalMaxPooling2D()(PreEff.output)
    dense = Dense(512)(x)
    if BN == True:
        dense = tf.keras.layers.BatchNormalization()(dense)
    dense = Activation(tf.keras.layers.PReLU())(dense)
    logits = ArcLayer(units = num_classes)(dense)
    
    model = Model(inputs = PreEff.inputs, outputs = logits)
    return  model

def ResNeXTArcLoss(Shape,num_classes,GEM = True,PoolType = 'GAP',BN=False,act='relu'):
    '''
    This is a model structure function, input a backbone and output model Structure.
    return Model(inputs = PreEff.inputs, outputs = logits)

    parameters discriptions
    - Shape => int type, input shape, ex : 224
    - num_classes => int type, number of class, ex : 10
    - GEM => bool type, use GeM layer or not, ex : True
    - PoolType => str type, if not using GEM , the flatten of using 'GAP' or 'GMP'
    - BN => bool type, using batch normalization or not
    - act => str type, activation function name, use keras activation function name or custom function mish
    '''

    inputs = tf.keras.layers.Input(shape=(Shape,Shape,3))
    layer = inputs
    filternum = 32

    #Convolution
    layer = Conv2D(filters=filternum,kernel_size=(7,7),strides=(2,2), padding='same')(layer)
    if BN == True:
        layer = tf.keras.layers.BatchNormalization()(layer)
    layer = Activation(act)(layer)

    #MaxPooling
    layer = MaxPooling2D((3, 3), strides=(2, 2),padding='same')(layer)

    #Residual Block 1
    layer = ResidualIConvXT(filter=filternum*2,BN=BN,attention = True,act=act)(layer)
    for i in range(2):#2
        layer = ResidualIdentityXT(filter=filternum*2,BN=BN,act=act)(layer)
    layer = MaxPooling2D((2, 2), strides=(2, 2))(layer)

    #Residual Block 2
    layer = ResidualIConvXT(filter=filternum*4,BN=BN,attention = True,act=act)(layer)
    for i in range(3):#3
        layer = ResidualIdentityXT(filter=filternum*4,BN=BN,act=act)(layer)
    layer = MaxPooling2D((2, 2), strides=(2, 2))(layer)

    #Residual Block 3
    layer = ResidualIConvXT(filter=filternum*8,BN=BN,attention = False,act=act)(layer)
    for i in range(22):#5
        layer = ResidualIdentityXT(filter=filternum*8,BN=BN,act=act)(layer)
    layer = MaxPooling2D((2, 2), strides=(2, 2))(layer)

    #Residual Block 4
    layer = ResidualIConvXT(filter=filternum*16,BN=BN,attention = False,act=act)(layer)
    for i in range(2):#2
        layer = ResidualIdentityXT(filter=filternum*16,BN=BN,act=act)(layer)
    _,H,W,_ = layer.get_shape().as_list()
    if GEM == True:
        layer = GeM(pool_size=H)(layer)
        dense = Flatten()(layer)
    elif GEM == False:
        if PoolType == 'GAP':
            dense = GlobalAveragePooling2D()(layer)
        elif PoolType == 'GMP':
            dense = GlobalMaxPooling2D()(layer)
    dense = Dense(512)(dense)
    if BN == True:
        dense = tf.keras.layers.BatchNormalization()(dense)
    dense = Activation(tf.keras.layers.PReLU())(dense)
    logits = ArcLayer(units = num_classes)(dense)
    model = Model(inputs = inputs, outputs = logits)
    return  model

def ResnetSE101(Shape,num_classes,GEM = True,PoolType = 'GAP',BN=False,act='relu'):
    '''
    This is a model structure function, input a backbone and output model Structure.
    return Model(inputs = PreEff.inputs, outputs = logits)

    parameters discriptions
    - Shape => int type, input shape, ex : 224
    - num_classes => int type, number of class, ex : 10
    - GEM => bool type, use GeM layer or not, ex : True
    - PoolType => str type, if not using GEM , the flatten of using 'GAP' or 'GMP'
    - BN => bool type, using batch normalization or not
    - act => str type, activation function name, use keras activation function name or custom function mish
    '''

    inputs = tf.keras.layers.Input(shape=(Shape,Shape,3))
    layer = inputs
    filternum = 32

    #Convolution
    layer = Conv2D(filters=filternum,kernel_size=(7,7),strides=(2,2), padding='same')(layer)
    if BN == True:
        layer = tf.keras.layers.BatchNormalization()(layer)
    layer = Activation(act)(layer)

    #MaxPooling
    layer = MaxPooling2D((3, 3), strides=(2, 2),padding='same')(layer)

    #Residual Block 1
    layer = ResidualIConvSE(filter=filternum*2,BN=BN,attention = True,act=act)(layer)
    for i in range(2):#2
        layer = ResidualIdentitySE(filter=filternum*2,BN=BN,act=act)(layer)
    layer = MaxPooling2D((2, 2), strides=(2, 2))(layer)

    #Residual Block 2
    layer = ResidualIConvSE(filter=filternum*4,BN=BN,attention = True,act=act)(layer)
    for i in range(3):#3
        layer = ResidualIdentitySE(filter=filternum*4,BN=BN,act=act)(layer)
    layer = MaxPooling2D((2, 2), strides=(2, 2))(layer)

    #Residual Block 3
    layer = ResidualIConvSE(filter=filternum*8,BN=BN,attention = False,act=act)(layer)
    for i in range(22):#5
        layer = ResidualIdentitySE(filter=filternum*8,BN=BN,act=act)(layer)
    layer = MaxPooling2D((2, 2), strides=(2, 2))(layer)

    #Residual Block 4
    layer = ResidualIConvSE(filter=filternum*16,BN=BN,attention = False,act=act)(layer)
    for i in range(2):#2
        layer = ResidualIdentitySE(filter=filternum*16,BN=BN,act=act)(layer)
    _,H,W,_ = layer.get_shape().as_list()
    if GEM == True:
        layer = GeM(pool_size=H)(layer)
        dense = Flatten()(layer)
    elif GEM == False:
        if PoolType == 'GAP':
            dense = GlobalAveragePooling2D()(layer)
        elif PoolType == 'GMP':
            dense = GlobalMaxPooling2D()(layer)
    dense = Dense(512)(dense)
    if BN == True:
        dense = tf.keras.layers.BatchNormalization()(dense)
    dense = Activation(tf.keras.layers.PReLU())(dense)
    logits = ArcLayer(units = num_classes)(dense)
    model = Model(inputs = inputs, outputs = logits)
    return  model

if __name__ == "__main__":
    Size = 224
    model = LocalAttentionFinetune(Shape = 512,num_classes = 81313,GEM = True,PoolType = 'GAP',BN=True,act='relu')
    #model = ResnetSE101(Shape = Size,num_classes = 10,GEM = True,PoolType = 'GAP',BN=True,act=mish)
    model.summary()





