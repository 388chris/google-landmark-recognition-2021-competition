import json
from lib_modules.Parameters import DataParameters, ModelStructParameters, ModelCompileParameters

class DicttoJson():
    def __init__(self,inputshape,modelname,GEM,PoolType,act):
        self.modelname = modelname
        self.inputshape = inputshape
        self.GEM = GEM
        self.PoolType = PoolType
        self.act = act
        
    def Tojson(self):
        # Data to be written
        if type(self.act) != str:
            actname = self.act.__name__
        else:
            actname = self.act
        ModelJson ={
            "modelname" : self.modelname,
            "imageshape" : self.inputshape,
            "GEM" : self.GEM,
            "PoolType" : self.PoolType,
            "act" : actname,}
        return ModelJson
    
    def Outputfile(self,outputname):
        Result = self.Tojson()
        with open(outputname, "w") as outfile:
            json.dump(Result, outfile)


