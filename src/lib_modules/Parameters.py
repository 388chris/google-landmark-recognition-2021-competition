from lib_models.mish_act import mish
from lib_models.Arc_Loss import ArcLoss
from lib_models.Focal_Loss import categorical_focal_loss
from Model_Struct import *

class DataParameters(object):
        # Data Location
        SaveCSVloc = '/work/u76024062/Dataset'
        SaveJson = '/home/u76024062/google_landmark/Chris_google-landmark-recognition-2021-competition/src/json'
        
        Trainsetdir = '/work/u76024062/Dataset/train' # Data Training set
        Traincsvloc = '/work/u76024062/Dataset/train.csv' # Train.csv
        Traindircsvloc = '/work/u76024062/Dataset/TrainingSet.csv' # Traindir.csv(Use this csv to do training)

        Testsetdir = '/work/u76024062/Dataset/test'   # Data Testing set
        Testdircsvloc = '/work/u76024062/Dataset/ValidationSet.csv' # Traindir.csv(Use this csv to do training)
        jsonfiletest = '/home/u76024062/google_landmark/Chris_google-landmark-recognition-2021-competition/src/json/PreEffiecientB7.json'
        
        #hyper parameters
        owngenerator = True       #use own generator for True, keras generator for False
        imageshape = 512          #images input shape
        batch_size =  32           # Training batch size
        shuffle = True             # Training set shuffle    
        modelname = 'ResNeXTArcLoss.h5' # The training Backbone CNN model name
        localattmodelname = 'ResNeXTArcLossGEMLocal.h5'  # The finetuning localattention model name


class ModelStructParameters(object):
        localattention = False           # True for using finetuning localattention, False for training Backbone CNN
        modelChoice = ResNeXTArcLoss     # Choice model structure    
        BatchNor = True           # Batch normalization
        GEM = True                # Use GEM for True, Use Pooltype function for False
        PoolType = 'GAP'          # GAP for global average pooling , GMP for global max pooling
        act = mish               # Choice mish or others keras activation
        
class ModelCompileParameters(object):
        MultiGPU = True         # Use multiple Gpu
        loss = ArcLoss()         # choice 'categorical_crossentropy' or ArcLoss()
        optimizerchoice = 'sgd'  # Choice the optimizer, 'sgd', 'adam' and 'RMSprop'
        learning_rate = 0.05      # Training learning_rate
        epochs = 30               # Training Epoch