import os
import numpy as np
import tensorflow as tf
from tensorflow.keras.utils import Sequence
from sklearn.utils import shuffle

class RandomCropDataGenerator(Sequence):

    def __init__(self, path, batch_size, size,labelinput,Shuffle=True):
        self.path = path
        self.batch_size = batch_size
        self.size = size
        self.Shuffle = Shuffle
        self.labelinput = labelinput   #labelinput = {'buildings':1,'sea':2, etc}
        self.num_classes = len(labelinput)
        self.n = len(path)
        self.on_epoch_end()

    def __len__(self):
        return int(np.floor(len(self.path) / self.batch_size))
        #return int(np.ceil(len(self.path) / float(self.batch_size)))

    def __getitem__(self, idx):
        path_batch = self.path[idx *self.batch_size:(idx + 1) * self.batch_size]
        X,y = self.__generator(path_batch)

        return X,y
    
    def on_epoch_end(self):
        if self.Shuffle == True:
            self.path = shuffle(self.path)
    
    def __generator(self, path_batch):
        concat_images = list()
        labels = list()

        for imgpath,label in path_batch:
#             img = tf.keras.preprocessing.image.load_img(imgpath,
#                                                         grayscale=False, 
#                                                         color_mode='rgb', 
#                                                         target_size=(self.size,self.size))

#             if img is None:
#                 concat_images.append(np.empty((self.size, self.size, 3)))
#                 print("Non image path:", imgpath)
#                 continue
                
#             img = tf.keras.preprocessing.image.img_to_array(img)
#             img = tf.keras.applications.efficientnet.preprocess_input(img)
            img = tf.convert_to_tensor(np.array(PIL.Image.open(imgpath).convert('RGB')))
    
            if img.shape[0] < img.shape[1]:
                small = img.shape[0]
            else:
                small = img.shape[1]
                
            img = tf.image.random_crop(img, size=[small, small, 3])
            img = tf.image.resize(img, [self.size,self.size],method='nearest')
            img = tf.keras.applications.efficientnet.preprocess_input(img)
            
            concat_images.append(img)
            labels.append(self.labelinput[label])
        #Check = np.asarray(concat_images)
        return np.asarray(concat_images),tf.keras.utils.to_categorical(np.asarray(labels), num_classes=self.num_classes)

class DataGenerator(Sequence):

    def __init__(self, path, batch_size, size,labelinput,Shuffle=True):
        self.path = path
        self.batch_size = batch_size
        self.size = size
        self.Shuffle = Shuffle
        self.labelinput = labelinput   #labelinput = {'buildings':1,'sea':2, etc}
        self.num_classes = len(labelinput)
        self.n = len(path)
        self.on_epoch_end()

    def __len__(self):
        return int(np.floor(len(self.path) / self.batch_size))
        #return int(np.ceil(len(self.path) / float(self.batch_size)))

    def __getitem__(self, idx):
        path_batch = self.path[idx *self.batch_size:(idx + 1) * self.batch_size]
        X,y = self.__generator(path_batch)

        return X,y
    
    def on_epoch_end(self):
        if self.Shuffle == True:
            self.path = shuffle(self.path)
    
    def __generator(self, path_batch):
        concat_images = list()
        labels = list()

        for imgpath,label in path_batch:
            img = tf.keras.preprocessing.image.load_img(imgpath,
                                                        grayscale=False, 
                                                        color_mode='rgb', 
                                                        target_size=(self.size,self.size))

            if img is None:
                concat_images.append(np.empty((self.size, self.size, 3)))
                print("Non image path:", imgpath)
                continue
                
            img = tf.keras.preprocessing.image.img_to_array(img)
            img = tf.keras.applications.efficientnet.preprocess_input(img)

            concat_images.append(img)
            labels.append(self.labelinput[label])
        Check = np.asarray(concat_images)
        return np.asarray(concat_images),tf.keras.utils.to_categorical(np.asarray(labels), num_classes=self.num_classes)

class ValidDataGenerator(Sequence):

    def __init__(self, path, batch_size, size):
        self.path = path
        self.batch_size = batch_size
        self.size = size
        self.n = len(path)
        self.count = 0

    def __len__(self):
        return int(np.floor(len(self.path) / self.batch_size))

    def __getitem__(self, idx):
        path_batch = self.path[idx *self.batch_size:(idx + 1) * self.batch_size]
        X= self.__generator(path_batch)

        return X
    
    def __generator(self, path_batch):
        for imgpath , _ in path_batch:
            img = tf.keras.preprocessing.image.load_img(imgpath,
                                                        grayscale=False, 
                                                        color_mode='rgb', 
                                                        target_size=(self.size,self.size))
                
            img = tf.keras.preprocessing.image.img_to_array(img)
            img = tf.keras.applications.efficientnet.preprocess_input(img)
            img = tf.expand_dims(img, axis=0)
            self.count += 1
            
            if self.count % 5000 == 0:
                print(self.count)
        return img
