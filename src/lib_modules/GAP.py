# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.11.3
#   kernelspec:
#     display_name: Christf24
#     language: python
#     name: christf24
# ---

import pandas as pd
import numpy as np


def global_average_precision_score(y_true, y_pred):
    """
    Compute Global Average Precision score (GAP)
    Parameters
    ----------
    y_true : Dictionary with query ids and true ids for query samples
             sample : {'17660ef415d37059': 1} -> {'Imagename' : label}
    y_pred : Dictionary with query ids and predictions (predicted id, confidence level)
             sample : {'17660ef415d37059': (1, 0.5422306879535601),} -> {'imagename' : (label, confidence score)}
    Returns
    -------
    float
        GAP score
    """
    indexes = list(y_pred.keys())
    indexes.sort(key=lambda x: -y_pred[x][1],)
    
    queries_with_target = len([i for i in y_true.values() if i is not None])
    correct_predictions = 0
    total_score = 0.
    
    for i, k in enumerate(indexes, 1):
        relevance_of_prediction_i = 0
        if y_true[k] == y_pred[k][0]:
            correct_predictions += 1
            relevance_of_prediction_i = 1
        precision_at_rank_i = correct_predictions / i
        total_score += precision_at_rank_i * relevance_of_prediction_i

    return 1 / queries_with_target * total_score
