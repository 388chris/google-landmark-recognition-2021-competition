import pandas as pd
import os

class dataLoader():
        def __init__(self, csvloc, datadir, savecsvdir):
            self.csvloc = csvloc
            self.datadir = datadir
            self.savecsvdir = savecsvdir
            self.TrainFile = pd.read_csv(csvloc)
        def ReadCSV(self):
                return self.TrainFile

        def showcsv(self):
                print("Total label(unique) : ",len(self.TrainFile['landmark_id'].unique()))
                print("Total Image :", len(self.TrainFile['id']))
                print(self.TrainFile.head())

        def FindLabels(self):
                label = {}
                index = 0
                for i in self.TrainFile['landmark_id'].unique():
                        label[i] = index
                        index += 1
                return label

        def Files2DF(self):
                x_data_list=[self.datadir +'/'+id[0]+'/'+id[1]+'/'+id[2]+'/'+id+'.jpg' for id in self.TrainFile['id']]
                y_data_list = self.TrainFile['landmark_id']
                data_list = pd.DataFrame({})
                data_list['img_path'] = x_data_list
                data_list['label'] = y_data_list
                data_list.to_csv(self.savecsvdir+'/Traindir.csv',index=False)


if __name__ == "__main__":
        SaveCSVloc = 'D:/Dataset/landmark-recognition-2021'
        Traincsvloc = 'D:/Dataset/landmark-recognition-2021/train.csv' # Train.csv
        Trainsetdir = 'D:/Dataset/landmark-recognition-2021/train' # Data Training set

        DataLoader = dataLoader(csvloc = Traincsvloc, 
        datadir = Trainsetdir,
        savecsvdir = SaveCSVloc)
        label = DataLoader.FindLabels()
        DataLoader.Files2DF()

