# -*- coding: utf-8 -*-
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from tensorflow.keras.models import Model


#label : label dictionary
class plot_grad():
    def __init__(self, model, label_dict, imgpath, target_size):
        '''
        model: keras model
        label_dict: dictionary, keys are index of labels and values are actual label, ex: labelindex.csv
        imgpath: string, the location of image
        target_size: tuple, ex: (512, 512)
        '''
        self.model = model
        self.label = label_dict
        self.imgpath = imgpath
        self.target_size = target_size
        self.img = self.read_img(imgpath)
        
    def read_img(self, imgpath):
        img = tf.keras.preprocessing.image.load_img(imgpath,
                                                        grayscale=False, 
                                                        color_mode='rgb', 
                                                        target_size=self.target_size)
        return img
        
    def preprocessing(self, img):
        img = tf.keras.preprocessing.image.img_to_array(img)
        img = tf.keras.applications.efficientnet.preprocess_input(img)
        return tf.expand_dims(img, axis=0)
    
    def gradcam(self, layer_name): 
        last_conv_layer = self.model.get_layer(layer_name)
        img = self.preprocessing(self.img)

        # Get the gradient model, input model and output last_conv_layer value and the probability of prediction
        grad_model = Model(
            inputs = self.model.inputs, 
            outputs = [last_conv_layer.output, self.model.output])

        with tf.GradientTape() as tape:
            last_conv_layer_output, preds = grad_model(img)
            pred_class = np.argmax(preds[0]) #prediction class
            pred_output = preds[:, pred_class] #prediction softmax output, the most highest number

        pred_class_name = self.label[pred_class]

        # 求得分類的神經元對於最後一層 convolution layer 的梯度, 維度就是最後一層的維度(1,7,7,1024)
        grads = tape.gradient(pred_output, last_conv_layer_output)

        # 求得針對每個 feature map 的梯度平均,維度就是(1024,)
        pooled_grads = tf.reduce_mean(grads, axis=(0, 1, 2))
        convs_list = []

        for i in range(pooled_grads.shape[0]):# 最後一層的feature去乘上pooled_grads (7,7,1024) * (1024)
            convs_list.append(last_conv_layer_output[0,:,:,i] * pooled_grads[i])

        heatmap = tf.reduce_mean(convs_list, axis=0) #heat map (7,7)

        #先做一個relu再用minmax將數值normalize到0,1之間
        heatmap = (tf.maximum(heatmap, 0) / tf.math.reduce_max(heatmap)) * 255
        
        return heatmap
    
    def plot(self, layer_name):
        '''
        layer_name: the layer to visualize, shown by model.summary() ex: 'Conv_4'
        '''
        heatmap = self.gradcam(layer_name)
        heatmap = np.uint8(heatmap)
        heatmap = np.expand_dims(heatmap, axis=2)
        heatmap = tf.image.resize(heatmap, self.target_size)

        plt.figure(figsize=(15, 5))
        plt.subplot(131)
        plt.imshow(self.img)
        plt.title(self.imgpath)
        
        plt.subplot(132)
        plt.imshow(self.img,alpha = 0.7)
        plt.imshow(heatmap, cmap='jet', alpha=0.3)
        plt.title('GradCam')
        
        plt.subplot(133)
        plt.imshow(heatmap, cmap='jet')
        plt.title('Heatmap')
        
        plt.show()





