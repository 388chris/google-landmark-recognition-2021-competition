import gc
import tensorflow as tf

class RamCleaning(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs=None):
        gc.collect()

        