import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Dropout, Dense, Activation,GlobalAveragePooling2D,GlobalMaxPooling2D
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.keras.layers import BatchNormalization
from lib_models.SE_block import SEblock

def ResidualIdentitySE(filter,BN=False,act='relu'):
    def Res(input):
        _,_,_,channels = input.get_shape().as_list()
        ConvIn = Conv2D(filters=filter,kernel_size=(1,1),strides=(1,1), padding='same')(input)
        if BN == True:
            ConvIn = tf.keras.layers.BatchNormalization()(ConvIn)
        ConvIn = Activation(act)(ConvIn)
        ConvSec = Conv2D(filters=filter,kernel_size=(3,3),strides=(1,1), padding='same')(ConvIn)
        if BN == True:
            ConvSec = tf.keras.layers.BatchNormalization()(ConvSec)
        
        ConvSec = Activation(act)(ConvSec)  
        ConvOut = Conv2D(filters=channels,kernel_size=(1,1),strides=(1,1), padding='same')(ConvSec)
        if BN == True:
            ConvOut = tf.keras.layers.BatchNormalization()(ConvOut)

        ConvOut = tf.keras.layers.Add()([input,ConvOut])
        ConvOut = Activation(act)(ConvOut)
        return ConvOut
    return Res
def ResidualIConvSE(filter,BN=False,attention = False,reduction_ratio = 16,act='relu'):
    def Res(input):
        ConvIn = Conv2D(filters=filter,kernel_size=(1,1),strides=(1,1), padding='same')(input)
        if BN == True:
            ConvIn = tf.keras.layers.BatchNormalization()(ConvIn)
        ConvIn = Activation(act)(ConvIn)
        if attention == True:
            ConvSec = SEblock(inputs = ConvIn,filters = filter,reduction_ratio = reduction_ratio)
        else:
            ConvSec = Conv2D(filters=filter,kernel_size=(3,3),strides=(1,1), padding='same')(ConvIn)
            if BN == True:
                ConvSec = tf.keras.layers.BatchNormalization()(ConvSec)
            ConvSec = Activation(act)(ConvSec)

        ConvOut = Conv2D(filters=filter*2,kernel_size=(1,1),strides=(1,1), padding='same')(ConvSec)
        if BN == True:
            ConvOut = tf.keras.layers.BatchNormalization()(ConvOut)

        Conv_init = Conv2D(filters=filter*2,kernel_size=(1,1),strides=(1,1), padding='same')(input)
        if BN == True:
            Conv_init = tf.keras.layers.BatchNormalization()(Conv_init)
        ConvOut = tf.keras.layers.Add()([Conv_init,ConvOut])
        ConvOut = Activation(act)(ConvOut)
        return ConvOut
    return Res