import tensorflow as tf
from tensorflow.keras.layers import Conv2D, Dense, Activation
from tensorflow.keras.layers import Input
import os
from tensorflow.keras.models import Model
import tensorflow.keras.backend as K

def global_context_block(inputs,reduction=16,bias = False,act = 'linear'):
    batch_size, height, width, num_channels = inputs.get_shape().as_list()
    #Context Block
    Conv = Conv2D(filters=1,
                  kernel_size=(1,1),
                  strides=(1,1), 
                  padding='same',
                  use_bias=bias)(inputs)#H,W,1
    
    Conv = tf.reshape(Conv,[-1,height*width,1])  #H*W,1
    ReshapeConv = tf.nn.softmax(Conv)            #H*W,1
    
    ReshapeInputs = tf.reshape(inputs,[-1,num_channels,height,width])  #c,H,W
    ReshapeInputs = tf.reshape(inputs,[-1,num_channels,height*width])  #c,H*W
    channel_output = tf.matmul(ReshapeInputs,Conv) #c,1
    channel_output = tf.reshape(channel_output,[-1,1,1,num_channels])#1,1,c
    
    #SE Block
    ConvS = Conv2D(filters=num_channels // reduction,
                    kernel_size=(1,1),
                    strides=(1,1),
                    padding='same',
                    use_bias = bias)(channel_output)#1,1,C//r
                    
    ConvS = tf.keras.layers.LayerNormalization()(ConvS)
    ConvS = Activation('relu')(ConvS)
    
    ConvE = Conv2D(filters=num_channels,
                    kernel_size=(1,1),
                    strides=(1,1),
                    padding='same',
                    activation = act)(ConvS)#1,1,C
    
    GCBOutput = tf.keras.layers.Add()([inputs,ConvE])#H,W,C(inputs的每個pixel都加上ConvE輸入的channel output)
    return GCBOutput

if __name__ == "__main__":
    Size = 28; channel = 3
    inputs = Input(shape=(Size,Size,channel))
    kernel = 5
    layer = Conv2D(filters=128,kernel_size=(3,3),strides=(1,1), padding='same',activation = 'relu')(inputs)
    layer = global_context_block(layer,16,bias = False,act = 'linear')
    model = Model(inputs=inputs, outputs = layer)
    model.summary()

