import tensorflow.keras.backend as K


def mish(x):
    return x*K.tanh(K.softplus(x))