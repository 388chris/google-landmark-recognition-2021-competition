import numpy as np
import tensorflow as tf
import tensorflow.keras.backend as K
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Reshape, Dense
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Softmax


def selfattention(inputs,filter,divide=2,bias=True,BN=False,name = 'First'):
    batch_size, height, width, num_channels = inputs.get_shape().as_list()
    Query = Conv2D(filters=filter // divide, kernel_size=(1,1), strides=(1,1), padding='same',use_bias=bias,name=name+'_conv_Query')(inputs)
    Key = Conv2D(filters=filter // divide, kernel_size=(1,1), strides=(1,1), padding='same',use_bias=bias,name=name+'_conv_Key')(inputs)
    Value = Conv2D(filters=filter // divide, kernel_size=(1,1), strides=(1,1), padding='same',use_bias=bias,name=name+'_conv_Vlaue')(inputs)

    Query = tf.reshape(Query,[-1,height*width,filter // divide]) #batchsize, height*width, channels
    Key = tf.transpose(tf.reshape(Key,[-1,height*width,filter // divide]),(0,2,1))
    Value = tf.reshape(Value,[-1,height*width,filter // divide])
    MultipleQK = tf.matmul(Query,Key)
    SoftmaxQK = tf.nn.softmax(MultipleQK)
    
    MultipleQKwithValue = tf.matmul(SoftmaxQK,Value)
    MultipleQKwithValue = tf.reshape(MultipleQKwithValue,[-1,height,width,filter // divide])
    
    MultipleQKwithValue = Conv2D(filters=num_channels, kernel_size=(1,1), strides=(1,1), padding='same',use_bias=bias,name = name + '_Conv_attention')(MultipleQKwithValue)
    if BN == True:
        MultipleQKwithValue = tf.keras.layers.BatchNormalization(name = name + '_BN_Conv_attention')(MultipleQKwithValue)
    Attention = tf.keras.layers.Add(name = name + '_Add_Attention')([MultipleQKwithValue,inputs])
    
    return Attention
