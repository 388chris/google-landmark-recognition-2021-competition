import tensorflow as tf
from tensorflow.keras.layers import Conv2D, Softmax, Layer, Flatten, Dense, Input
from tensorflow.keras.layers import  Activation
from tensorflow.keras.models import Model
import tensorflow.keras.backend as K
from tensorflow.keras import regularizers
import math
from tensorflow import keras

class GeM(tf.keras.layers.Layer):
    def __init__(self, pool_size, init_norm=3.0, normalize=False, **kwargs):
        self.pool_size = pool_size
        self.init_norm = init_norm
        self.normalize = normalize

        super(GeM, self).__init__(**kwargs)

    def get_config(self):
        config = super().get_config().copy()
        config.update({
            'pool_size': self.pool_size,
            'init_norm': self.init_norm,
            'normalize': self.normalize,
        })
        return config

    def build(self, input_shape):
        feature_size = input_shape[-1]
        self.p = self.add_weight(name='norms', shape=(feature_size,),
                                 initializer=keras.initializers.constant(self.init_norm),
                                 trainable=True)
        super(GeM, self).build(input_shape)

    def call(self, inputs):
        x = inputs
        x = tf.math.maximum(x, 1e-6)
        x = tf.pow(x, self.p)

        x = tf.nn.avg_pool(x, self.pool_size, self.pool_size, 'VALID')
        x = tf.pow(x, 1.0 / self.p)

        if self.normalize:
            x = tf.nn.l2_normalize(x, 1)
        return x

    def compute_output_shape(self, input_shape):
        return tuple([None, input_shape[-1]])

  
 

if __name__ == "__main__":
    Shape = 224
    inputs = tf.keras.layers.Input(shape=(Shape,Shape,3))
    layer = inputs
    layer = Conv2D(filters=32,kernel_size=(3,3),strides=(1,1), padding='same')(layer)
    layer = GeM(pool_size=2)(Layer)
    x = Flatten()(layer)
    x = Dense(10,activation='softmax')(x)
    model = Model(inputs,x)
    model.summary()
