import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import  Dense, Activation,GlobalAveragePooling2D,Reshape

def SEblock(inputs,filters,reduction_ratio = 16):
    squeeze = GlobalAveragePooling2D()(inputs)
    excitation = Reshape((1,1,filters))(squeeze)
    excitation = Dense(filters // reduction_ratio,activation = 'relu')(excitation)
    excitation = Dense(filters,activation = 'sigmoid')(excitation)
    
    scale = tf.multiply(squeeze, excitation)

    return scale

