import tensorflow as tf
from tensorflow.keras.layers import Conv2D, Conv3D, Softmax, Lambda, Add, Layer
from tensorflow.keras.layers import  Reshape, BatchNormalization, Activation
from tensorflow.keras.models import Model
import tensorflow.keras.backend as K
from einops.layers.tensorflow import Rearrange
from tensorflow.keras import initializers
from tensorflow import einsum, nn, meshgrid

def LambdaLayer(inputs,dim_k,n = None,r = None,heads = 4,dim_out = None,dim_u = 1):
    
    b, hh, ww, c, u, h = *inputs.get_shape().as_list(), dim_u, heads
    dim_v = dim_out // heads
    
    query = Conv2D(filters=dim_k * heads,kernel_size = 1, use_bias=False)(inputs) #h,w,dim_k * heads
    query = BatchNormalization()(query)
    
    key = Conv2D(filters=dim_k * dim_u,kernel_size = 1, use_bias=False)(inputs)  #h,w,dim_k * dim_u
    
    value = Conv2D(filters=dim_v * dim_u,kernel_size = 1, use_bias=False)(inputs)#h,w,dim_k * dim_u
    value = BatchNormalization()(value)
    
    query = Rearrange('b hh ww (h k) -> b h k (hh ww)', h=h)(query) #h,dim_k,h*w
    key = Rearrange('b hh ww (u k) -> b u k (hh ww)', u=u)(key)     #dim_u,dim_k,h*w
    value = Rearrange('b hh ww (u v) -> b u v (hh ww)', u=u)(value) #dim_u,dim_v,h*w
    
    key = tf.nn.softmax(key, axis=-1)
    
    Lc = einsum('b u k m, b u v m -> b k v', key, value) # dim_k,dim_v
    Yc = einsum('b h k n, b k v -> b n h v', query, Lc)  #H*W,head,dim_v
    
    if r is not None:#local context
        assert (r % 2) == 1, 'Receptive kernel size should be odd'
        value = Rearrange('b u v (hh ww) -> b v hh ww u', hh=hh, ww=ww)(value)#dim_u,dim_v,h*w -> dim_v, h, w, dim_u
        Lp = Conv3D(filters=dim_k, kernel_size=(1, r, r), padding='same')(value)
        Lp = Rearrange('b v h w k -> b v k (h w)')(Lp)
        Yp = einsum('b h k n, b v k n -> b n h v', query, Lp)
        
    Y = Yc + Yp
    out = Rearrange('b (hh ww) h v -> b hh ww (h v)', hh = hh, ww = ww)(Y)
    return out

if __name__ == "__main__":
    inputs = tf.keras.layers.Input(shape=(32,32,3))
    layer = inputs
    logits = LambdaLayer(inputs = layer,
                        dim_k = 16,
                        r = 23,
                        heads = 4,
                        dim_out = 64,
                        dim_u = 1)
    model = Model(inputs=inputs, outputs = logits)
    model.summary()