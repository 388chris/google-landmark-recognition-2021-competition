# Google landmark recognition 2021 competition

## Kaggle GLR2021
[https://www.kaggle.com/c/landmark-recognition-2021](https://www.kaggle.com/c/landmark-recognition-2021)


## I. How to train
   Change all of the parameters in **`lib_modules.Parameters.py`**
   ```bash
   python3 Main.py --t train
   ```

## II. How to test
   ```bash
   python3 Main.py --t test
   ```
## III. Main Folder Description
1. `model_link.csv`: the directory of models on Google drive
2. `TrainingSet.csv`: 1,422,423 data
3. `Validation.csv`: 158,047 data
    - Stratified by label  
4. `labelindex.csv`: 83131 data
   - the train index and label dictionary csv

## IV. `src` Folder Description

1. `Main.py` : Main function of the Code, execute and do all training progress
2. `Training.py` : Class function of TrainingOption, do all training progress.
   - `SaveJsondir` : Save the Json file directory
   - `owngenerator` : **default `False`**, Use own generator for True, keras generator for False.
   - `img_shape` : **default `224`**, input training shape 
   - `batch size` : **default `16`**, training batch size
   - `TrainCsv` : Input Training set CSV
   - `TestCsv` : Input Testing set CSV
   - `shuffle` : **default `True`**,Training set shuffle will be True, not shuffle will be False
   - `MultiGPU` : **default `True`**, Use multi GPU for True, one GPU for False
   - `modelChoice` : **default `ResNeXT`**, Choice the model structure in Model_Struct.py
   - `GEM` : **default `True` **, Use GEM layer flatten for True, use 'GAP' or 'GMP' flatten for False
   - `PoolType` : **default `'GAP'` **, if not using GEM , choose 'GAP' for global average pooling 2D,  choose 'GMP' for global max pooling 2D
   - `BatchNor` : **default `False`**, Use Batch normalization for true
   - `epoch` : **default `50`**, Training epochs
   - `learning_rate` : **default `1e-3`**, Training learning rate
   - `loss` : **default `'categorical crossentropy'`**, Training loss function
   - `optimizerchoice` : **default `sgd`**, Optimizer choice, `sgd`, `adam` and `RMSprop` to choice
   - `act` : **default `'relu'`**, model non linear activation function<br /> <br />

3. `Testing.py` : Class function of TestingOption, do all testing progress, use validation csv to valid and output the GAP score
   - `img_shape` : **default `224`**, input training shape 
   - `batch size` : **default `16`**, training batch size
   - `TrainCsv` : Input Training set CSV
   - `ValidCsv` : Input validation set CSV
   - `modelfiles` : **default `'EffiecientB7Pre.h5'`**, trained finish model dir and name <br /> <br />   
4. `Model_Structure.py` : Model Structure of the competition

   - Define a model Structure function with `Shpae`, `num_classes`, `GEM`, `PoolType`, `BN` and `act`, for example:
      - `Shape` : input shape
      - `num_classes` : number of classes
      - `GEM` : **default `True` **, Use GEM layer flatten for True, use 'GAP' or 'GMP' flatten for False
      - `PoolType` : **default `'GAP'` **, if not using GEM , choose 'GAP' for global average pooling 2D,  choose 'GMP' for global max pooling 2D
      - `BN` : **default `False`**, True for using batch normalization
      - `act` : **default `'relu'`**, model non linear activation function
      
   ```bash
   def ResNeXTArcLoss(Shape,num_classes,GEM = True,PoolType = 'GAP',BN=False,act='relu'):
      inputs = tf.keras.layers.Input(shape=(Shape,Shape,3))
      #model description
      logits = Dense(num_classes, activation='softmax')(dense)
      model = Model(inputs = inputs, outputs = logits)
      return model
   ```
   - ii. Make sure the flatten function use GEM layer or not, please define to the variable to `GEM`, for example:
   ```bash
    _,H,W,_ = layer.get_shape().as_list()
    if GEM == True:
        layer = GeM(pool_size=H)(layer)
        dense = Flatten()(layer)
    elif GEM == False:
        if PoolType == 'GAP':
            dense = GlobalAveragePooling2D()(layer)
        elif PoolType == 'GMP':
            dense = GlobalMaxPooling2D()(layer)
   ```
   - i. Make sure the Global pooling function use GAP layer or GMP, please define to the variable to `PoolType`, for example:
   ```bash
   if PoolType == 'GAP':
      dense = GlobalAveragePooling2D()(layer)
   elif PoolType == 'GMP':
      dense = GlobalMaxPooling2D()(layer)
   ``` 
   - iii. Make sure the layer will use batch normalization or not, please define to the varibale to `BN`, for example:
   ```bash
   if BN == True:
      layer = tf.keras.layers.BatchNormalization()(layer)
   ```
   - iv. if there is a hyper parameters of Activation function, please define to the variabke to `act`, for example:
   ```bash
   layer = tf.keras.layers.Activation(act)(layer)
   ```
   - v. make sure your output dense layer `num_classes`, this will be your softmax output, for example:
   ```bash
   logits = Dense(num_classes, activation='softmax')(dense)
   ```
5. `GLR2021_Submit.ipynb` : An example of kaggle GLR2021 submit notebook file
   
## V. `lib_models` Folder Description
1. `Arc_Loss.py` : custom layer and loss function of `ArcLayer` and `ArcLoss`
2. `Focal_Loss.py` : custom loss function of categorical focal loss
3. `Global_context_block.py` : custum block of global context block
   - `inputs` : input layer
   - `reduction` : **default `16`**, reduction ratio of dense layer 
   - `bias` : **default `False`**, bias of tf.keras.layers.Conv2D of global context block
   - `act` : **default `linear`**, the output activation function
   ```bash
   layer = global_context_block(layer,16,bias = False,act = 'linear')
   ```
4. `SE_block.py` : custom block of Squeeze and Excitation Block
   - `inputs` : input layer
   - `filter` : filters number
   - `reduction` : **default `16`**, reduction ratio of dense layer 
   ```bash
   layer = SEblock(inputs = layer,filters = 128,reduction_ratio = 16)
   ```   
5. `ResidualXT.py` : custom block of ResidualXT, including `ResidualIdentityXT` and `ResidualIConvXT`
   - `filter` : convolution filter number
   - `BN` :  **default `False`**, Use Batch normalization for true
   - `act` :  **default `'relu'`**, model non linear activation function
   - `input` : input layer
   - **Usage of ResidualIdentityXT**
   ```bash
   layer = ResidualIdentityXT(filter=64,BN=False,act='relu')(layer)
   ```
   - **Usage of ResidualConvXT**
   ```bash
   layer = ResidualConvXT(filter=64,BN=False,act='relu')(layer)
   ```
6. `ResidualSE.py` : custom block of ResidualSE, including `ResidualIdentitySE` and `ResidualIConvSE`
   - `filter` : convolution filter number
   - `BN` :  **default `False`**, Use Batch normalization for true
   - `act` :  **default `'relu'`**, model non linear activation function
   - `input` : input layer
   - **Usage of ResidualIdentityXT**
   ```bash
   layer = ResidualIdentitySE(filter=64,BN=False,act='relu')(layer)
   ```
   - **Usage of ResidualConvSE**
   ```bash
   layer = ResidualConvSE(filter=64,BN=False,act='relu')(layer)
   ```
7. `lambda_network.py` : custom layer of lamda_network, usage example:
   - `inputs` : input layers
   - `dim_k` : **default `16`**
   - `r` : **default `23`**
   - `heads` : **default `4`**, number head of multi head
   - `dim_out` : **default `None`**, output shape
   - `dim_u` : **default `1`** 
   ```bash
   logits = LambdaLayer(inputs = layer,
                  dim_k = 16,
                  r = 23,
                  heads = 4,
                  dim_out = 64,
                  dim_u = 1)
   ```
8. `mish.py` : custom activation function mish, usage example:
   ```bash
   layer = tf.keras.layers.Activation(mish)(layer)
   ```
   
## VI. `lib_modules` Folder Description
1. `CSVLoader.py` : Change the Default GLR2021 **Train.csv** to **Traindir.csv**, including direction location img and label
   - Example output of Traindir.csv
      |    | img_path | label | 
      |----|---------|:-------:| 
      |01 | D:\Dataset\landmark-recognition-2021\train\0\0\0\0000059611c7d079.jpg   |  107382  | 
      |02 | D:\Dataset\landmark-recognition-2021\train\0\0\0\000014b1f770f640.jpg   |  15430  | 
      |03 | D:\Dataset\landmark-recognition-2021\train\0\0\0\000015f76534add3.jpg   |  158262  | <br /> <br />
2. `Parameters.py` :
* Data based parameters of the code will in the class `DataParameters`
   - `SaveCSVloc` : Saving output **Traindir.csv** and **Testdir.csv** location 
   - `Trainsetdir` : **image Training set** location
   - `Traincsvloc` : GLR2021 default **Train.csv**
   - `Traindircsvloc` : Input to training progress **Traindir.csv**
   - `Testsetdir` : **image Testing set** location
   - `Testdircsvloc` : Input to testing progress **Testdir.csv**
   - `owngenerator` : **default `False`**, Use own generator for True, keras generator for False.
   - `imageshape` : **default `600`**, input training shape 
   - `batch size` : **default `16`**, training batch size
   - `shuffle` : **default `True`**, Training set shuffle<br /> <br />
* Model Structure parameters will in the class `ModelStructParameters`
   - `modelChoice` : **default `PreEffiecientB7`**, Choose model structure, PreEffiecientB7,PreEffiecientB3,PreDenseNet and ResNeXTArcLoss can be choose
   - `BatchNor` : **default `False`**, Use Batch normalization for true
   - `act` : **default `'relu'`**, model non linear activation function<br /> <br />
* Model compile parameters in `ModelCompileParameters`
   - `loss` : **default `'categorical crossentropy'`**, Training loss function, 'categorical_crossentropy' or ArcLoss()
   - `optimizerchoice` : **default `sgd`**, Optimizer choice, `sgd`, `adam` and `RMSprop` to choice
   - `learning_rate` : **default `1e-3`**, Training learning_rate
   - `epochs` : **default `30`**, Training Epoch<br /> <br />

3. `Ram_Clean.py` : Custom function of Ram cleaning, train an epoch and clean ram once.<br /> <br />
4. `data_generator.py` : Custom function of data generator
   - `DataGenerator` : Class function for training process generator
      - `path` : input two-dimension numpy array, data format will be [['im1.jpg',label_im1],['im2.jpg',label_im2]]
      - `batch_size` : batch size
      - `size` : output image size
      - `labelinput` : labelinput example, **{'label_name':index, 'buildings':1,'sea':2, ...}**
      - `Shuffle` : **default `True`**,Training set shuffle will be True, not shuffle will be False <br /> <br />
   - `ValidDataGenerator` : Class function for training process generator
      - `path` : input two-dimension numpy array, data format will be [['im1.jpg',label_im1],['im2.jpg',label_im2]]
      - `batch_size` : batch size
      - `size` : output image size<br /> <br />     
5. `GAP.py` : Custom function to compute Global Average Precision score (GAP)
   - `y_true` : dictionary with query ids and true ids for query samples
   - `y_pred` : dictionary with query ids and predictions (predicted id, confidence level)

6. `ParameterJson.py` : Custom function to change parameters dictionary to Json file
   - `modelname` : the input model name, example : 'PreEffiecientB7GEM.h5'
   - `inputshape` : the input image shape, example : 512
   - `GEM` : GEM function, True or False, example : True
   - `PoolType` : PoolType function, 'GAP' or 'GMP', example : 'GAP'
   - `act` : act function, if it is a function name, then change function to str name, example : mish -> 'mish'
   ```bash
   Outputjsonfile = DicttoJson(inputshape = 512,
                                 modelname = 'PreEffiecientB7GEM.h5',
                                 GEM = True,
                                 PoolType = 'GAP',
                                 act = 'relu',)
   Outputjsonfile.Outputfile('output.json') 
   ```

## VII. Pip version of code
|    | Package | Version | 
|----|---------|:-------:| 
|01 | Numpy   |  1.19.5  | 
|02 | opencv  |  4.5.1  |
|03 | pandas  |  1.2.4  |
|04 | scikit-learn| 0.24.2 |
|05 | Tensorflow-gpu   |  2.4.0  | 
|06 | einops  |  0.3.0  |
